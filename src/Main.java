import javax.swing.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import static Constants.Constants.CHAT_GPT_API_KEY;
import static Constants.Constants.CHAT_GPT_ENDPOINT;
import static Constants.Constants.CHAT_GPT_MODEL;


//TODO
//1)Train AI to look at specific/custom data  - see "fine-tuned"
//step 1: teach ai to read information/learn from specific source material
//step 2: use that document for responses
//2)need to fix formatting on long paragraph answers
//3)import log4j

//Questions
//why won't spanish responses work

//Simple Application to access Open AI API
public class Main {
    public static void main(String[] args) {
        Integer questionCount = 1;
        Boolean askAQuestion = textBoxLogic(questionCount);

        if(askAQuestion){
            questionCount = 2;
            textBoxLogic(questionCount);
        }
    }

    public static boolean textBoxLogic(Integer questionCount){
        String userQuestion;
        if(questionCount==1) {
            userQuestion = JOptionPane.showInputDialog("Hello, I am Keith's AI interface application. Ask me something!");
        }else{
            userQuestion = JOptionPane.showInputDialog("Lets go again! Ask something else");
        }
        String testQuestion = userQuestion;
        String chatGPTResponse = chatGPT(testQuestion);
        String chatGPTResponseRemoveBreaks = chatGPTResponse.replace("\n", "");
        String chatGPTResponseBreakAtSentence=chatGPTResponseRemoveBreaks.replace(".", ". \n");
        String formattedTextBoxResponse = "User Question: "+userQuestion+"\nAI:\n"+chatGPTResponseBreakAtSentence+"\n\n"+"Can I help you with anything else?";
        System.out.println("\n\n-------START Testing Chat GPT API-------\n");
        //showInputDialog
        int dialogButton = JOptionPane.showConfirmDialog (null, formattedTextBoxResponse,"Jarvis Chat Bot", JOptionPane.YES_NO_OPTION);
        if(dialogButton == JOptionPane.YES_OPTION) {
            System.out.println(chatGPTResponse);
            questionCount++;
            return true;
        }
        return false;
    }

    public static String chatGPT(String message){
        String url = CHAT_GPT_ENDPOINT;
        String apiKey = CHAT_GPT_API_KEY;
        //Get API key from "platform.openai.com/account/api-keys"
        String model = CHAT_GPT_MODEL;
        try{
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Authorization", "Bearer " + apiKey);
            con.setRequestProperty("Content-Type", "application/json");
            //Build body of the request
            String body = "{\"model\": \"" + model + "\", \"messages\": [{\"role\": \"user\", \"content\": \"" + message + "\"}]}";
            con.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter((con.getOutputStream()));
            writer.write(body);
            writer.flush();
            writer.close();
            //Retrieve response
            BufferedReader in = new BufferedReader((new InputStreamReader(con.getInputStream())));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null){response.append(inputLine);}
            in.close();
            return extractContentFromResponse(response.toString());
        } catch (IOException e){
            throw  new RuntimeException(e);
        }
    }
    // This method extracts the response expected from chatgpt and returns it.
    public static String extractContentFromResponse(String response) {
        int startMarker = response.indexOf("content")+11; // Marker for where the content starts.
        int endMarker = response.indexOf("\"", startMarker); // Marker for where the content ends.
        return response.substring(startMarker, endMarker); // Returns the substring containing only the response.
    }
}